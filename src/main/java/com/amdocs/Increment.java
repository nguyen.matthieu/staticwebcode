package com.amdocs;

public class Increment {
	
	private static int counter = 1;

	public Increment () {
	}
	
	public int getCounter() {
	    return counter++;
	}
			
	public int decreasecounter(final int input) {
            int result;
            if (input == 0){
	        result = counter--;
	    } else {
                if (input == 1) {
                    result = counter;
		} else {
		    result = counter++;
		}
	    }
            return result;
	}			
}
