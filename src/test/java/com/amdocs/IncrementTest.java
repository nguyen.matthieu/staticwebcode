package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {

    public IncrementTest() {
    }

    @Test
    public void testDecreaseCounterBy1() throws Exception {
        final Increment increment = new Increment();
        final int originalCounter = increment.decreasecounter(1);
        final int result = increment.decreasecounter(1);
        assertEquals("decreaseCounter(1) leaves the counter unchanged", originalCounter, result);
    }

    @Test
    public void testDecreaseCounterBy10() throws Exception {
        final Increment increment = new Increment();
        final int originalCounter = increment.decreasecounter(1);
        increment.decreasecounter(10);
        assertEquals("decreaseCounter(10) increments the counter by 1", originalCounter + 1, increment.decreasecounter(1));
    }

    @Test
    public void testDecreaseCounterBy0() throws Exception {
        final Increment increment = new Increment();
        final int originalCounter = increment.decreasecounter(1);
        increment.decreasecounter(0);
        assertEquals("decreaseCounter(0) decrements the counter by 1", originalCounter - 1, increment.decreasecounter(1));
    }


}

